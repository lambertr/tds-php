<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use DateTime;

class ControleurTrajet{
    public static function afficherDetail() : void {
        $trajet = (new TrajetRepository())->recupererParClePrimaire($_GET['id']);
        if($trajet == NULL) {
            self::afficherErreur("Le trajet n'existe pas");
        } else {
            self::afficherVue('vueGenerale.php', ['trajet' => $trajet, "titre" => "Détail du trajet", "cheminCorpsVue" => "trajet/detail.php"]);
        }
    }
    public static function afficherListe() : void {
        $trajets = (new TrajetRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php',["trajets"=>$trajets,"titre" => "Liste des trajets", "cheminCorpsVue" => "trajet/liste.php"]);
    }
    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ .  "/../vue/$cheminVue"; // Charge la vue
    }
    public static function afficherErreur(string $messageErreur = "") : void{
        self::afficherVue('vueGenerale.php',["messageErreur"=> $messageErreur ,"titre" => "Erreur", "cheminCorpsVue" => "trajet/erreur.php"]);
    }

    public static function supprimer() : void{
        $id = htmlspecialchars($_GET['id']);
        (new TrajetRepository)->supprimer($id);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue('vueGenerale.php',['trajets' => $trajets,'id' => $id, "titre" => "Trajet supprimer", "cheminCorpsVue" => "trajet/trajetSupprime.php"]);
    }
    public static function afficherFormulaireCreation() : void {
        self::afficherVue('vueGenerale.php',["titre" => "Formulaire de création", "cheminCorpsVue" => "trajet/formulaireCreation.php"]);
    }
    public static function creerDepuisFormulaire() : void
    {
        $trajet = self::construireDepuisFormulaire($_GET);
        $trajetRepository = new TrajetRepository();
        $trajetRepository->ajouter($trajet);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ['trajets' => $trajets, 'titre' => "Trajet créé", 'cheminCorpsVue' => "trajet/trajetCree.php"]);
    }
    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Trajet
    {
        $loginConducteur = htmlspecialchars($tableauDonneesFormulaire['conducteurLogin']);
        $utilisateurRepository = new UtilisateurRepository();
        $conducteur = $utilisateurRepository->recupererParClePrimaire($loginConducteur);
        $date = new DateTime(htmlspecialchars($tableauDonneesFormulaire['date']));
        $id = $tableauDonneesFormulaire['id'] ?? null;
        return new Trajet(
            $id,
            htmlspecialchars($tableauDonneesFormulaire['depart']),
            htmlspecialchars($tableauDonneesFormulaire['arrivee']),
            $date,
            htmlspecialchars($tableauDonneesFormulaire['prix']),
            $conducteur,
            isset($tableauDonneesFormulaire['nonFumeur']) ? 0 : 1
        );
    }

    public static function mettreAJour() : void
    {
        $trajet = self::construireDepuisFormulaire($_GET);
        $trajetRepository = new TrajetRepository();
        $trajetRepository->mettreAJour($trajet);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ['id'=> htmlspecialchars($_GET['id']),'trajets' => $trajets, 'titre' => "Trajet mis à jour", 'cheminCorpsVue' => "trajet/trajetMisAJour.php"]);
    }
    public static function afficherFormulaireMiseAJour() : void{
        $id = htmlspecialchars($_GET['id']);
        $trajet = (new TrajetRepository())->recupererParClePrimaire($id);
        self::afficherVue('vueGenerale.php', ['trajet' => $trajet, "titre" => "Mise à jour Trajet", "cheminCorpsVue" => "trajet/formulaireMiseAJour.php"] );
    }

}