<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Details des utilisateurs</title>
</head>
<body>
<?php

/** @var Utilisateur[] $utilisateur */

$prenom = htmlspecialchars($utilisateur->getPrenom(), ENT_QUOTES, 'UTF-8');
$nom = htmlspecialchars($utilisateur->getNom(), ENT_QUOTES, 'UTF-8');
$login = htmlspecialchars($utilisateur->getLogin(), ENT_QUOTES, 'UTF-8');

echo "<p> L'utilisateur $prenom $nom avec le login $login.</p>";
?>
</body>
</html>