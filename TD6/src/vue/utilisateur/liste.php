<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Liste des utilisateurs</title>
</head>
<body>
<?php
$utilisateursURL = [];
/** @var Utilisateur[] $utilisateurs */

foreach ($utilisateurs as $utilisateur) {
    $loginUrl = $utilisateur->getLogin();
    echo '<p> Utilisateur de login ' . '<a href="controleurFrontal.php?action=afficherDetail&controleur=utilisateur&login=' . rawurlencode($utilisateur->getLogin()) . '">' . $loginUrl . '</a> '
        .'<a href="controleurFrontal.php?action=supprimer&controleur=utilisateur&login=' . rawurlencode($utilisateur->getLogin()) . '"> Supprimer </a> '
        .'<a href="controleurFrontal.php?action=afficherFormulaireMiseAJour&controleur=utilisateur&login=' . rawurlencode($utilisateur->getLogin()) . '"> Modifier </a> ' .'</p>'
    ;
}
echo '<p> ' . '<a href="controleurFrontal.php?action=afficherFormulaireCreation&controleur=utilisateur" > Créer un utilisateur</a>';

?>
</body>
</html>
