<?php
/** @var Utilisateur $utilisateur */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title> Mon premier php </title>
</head>
<body>
<form method="get" action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?= htmlspecialchars($utilisateur->getLogin())?>" name="login" id="login_id" readonly="readonly" required>

        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?= htmlspecialchars($utilisateur->getNom())?>" name="nom" id="nom_id" required>

        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prenom&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?= htmlspecialchars($utilisateur->getPrenom())?>" name="prenom" id="prenom_id" required>

        </p>
        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
    <input type='hidden' name='action' value='mettreAJour'>
    <input type='hidden' name='controleur' value='utilisateur'>
</form>
</body>
</html>