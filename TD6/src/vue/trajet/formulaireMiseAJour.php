<?php
/** @var Trajet $trajet */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title> Mise à jour du trajet </title>
</head>
<body>
<div>
    <form method="get" action="controleurFrontal.php">
        <fieldset>
            <legend>Mettre à jour un trajet :</legend>
            <p>
                <label for="depart_id">Départ</label> :
                <input type="text" name="depart" id="depart_id" value="<?= htmlspecialchars($trajet->getDepart()) ?>" required/>
            </p>
            <p>
                <label for="arrivee_id">Arrivée</label> :
                <input type="text" name="arrivee" id="arrivee_id" value="<?= htmlspecialchars($trajet->getArrivee()) ?>" required/>
            </p>
            <p>
                <label for="date_id">Date</label> :
                <input type="date" name="date" id="date_id" value="<?= htmlspecialchars($trajet->getDate()->format('Y-m-d')) ?>" required/>
            </p>
            <p>
                <label for="prix_id">Prix</label> :
                <input type="number" name="prix" id="prix_id" value="<?= htmlspecialchars($trajet->getPrix()) ?>" required/>
            </p>
            <p>
                <label for="conducteurLogin_id">Login du conducteur</label> :
                <input type="text" name="conducteurLogin" id="conducteurLogin_id" value="<?= htmlspecialchars($trajet->getConducteur()->getLogin()) ?>" required/>
            </p>
            <p>
                <label for="nonFumeur_id">Non Fumeur ?</label> :
                <input type="checkbox" name="nonFumeur" id="nonFumeur_id" <?= $trajet->isNonFumeur() ? 'checked' : '' ?> />
            </p>
            <p>
                <input type="submit" value="Mettre à jour" />
            </p>
        </fieldset>
        <input type="hidden" name="id" value="<?= htmlspecialchars($trajet->getId()) ?>">
        <input type='hidden' name='action' value='mettreAJour'>
        <input type='hidden' name='controleur' value='trajet'>
    </form>
</div>
</body>
</html>
