<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\Utilisateur;
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = Utilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php',["utilisateurs"=>$utilisateurs,"titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);
    }
    public static function afficherDetail() : void {
        $utilisateur = Utilisateur::recupererUtilisateurParLogin($_GET['login']);
        if($utilisateur == NULL) {
            self::afficherVue('vueGenerale.php',["titre" => "erreur", "cheminCorpsVue" => "utilisateur/erreur.php"]);
        } else{
            self::afficherVue('vueGenerale.php',['utilisateur' => $utilisateur,"titre" => "Detail de l'utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);
        }
    }
    public static function afficherFormulaireCreation() : void {
        self::afficherVue('vueGenerale.php',["titre" => "Formulaire de création", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }
    public static function creerDepuisFormulaire() : void{
        $nom = htmlspecialchars($_GET['nom']);
        $prenom = htmlspecialchars($_GET['prenom']);
        $login = htmlspecialchars($_GET['login']);
        $utilisateur = new Utilisateur($login, $nom, $prenom);
        $utilisateur->ajouter();
        $utilisateurs = Utilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php',['utilisateurs' => $utilisateurs,"titre" => "Utilisateur crée", "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
    }
    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ .  "/../vue/$cheminVue"; // Charge la vue
    }


}
?>