<?php
namespace App\Covoiturage\Modele;
use App\Covoiturage\Configuration\ConfigurationBaseDeDonnees;
class ModeleUtilisateur {

    private string $login;
    private string $nom;
    private string $prenom;

    // un getter
    public function getNom() {
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom) {
        $this->nom = $nom;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function setLogin(string $login)
    {
        $this->login = substr($login, 0, 64);
    }
    public function getPrenom()
    {
        return $this->prenom;
    }
    public function setPrenom(string $prenom)
    {
        $this->prenom = $prenom;
    }


    // un constructeur
    public function __construct(
        string $login,
        string $nom,
        string $prenom,
   )
    {
        $this->login = substr($login, 0, 64);
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    /*
    public function __toString() {
        return "Utilisateur [Login: " . $this->login .
            ", Nom: " . $this->nom .
            ", Prénom: " . $this->prenom . "]";
    }
    */

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : Utilisateur {
        return new self(
            $utilisateurFormatTableau['login'],
            $utilisateurFormatTableau['nom'],
            $utilisateurFormatTableau['prenom']
        );
    }
    public static function recupererUtilisateurs(){
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query('SELECT * FROM utilisateur');
        $tableauUtilisateurs = [];
        foreach($pdoStatement as $utilisateurFormatTableau){
            $utilisateur = Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
            $tableauUtilisateurs[] = $utilisateur;
        }
        return $tableauUtilisateurs;
    }
    public static function recupererUtilisateurParLogin(string $login) : ?Utilisateur {
        $sql = "SELECT * from utilisateur WHERE login = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();
        if($utilisateurFormatTableau){
            return Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
        }
        return null;
    }

    public function ajouter() : void
    {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $pdoStatement = $pdo->prepare('INSERT INTO utilisateur (login, nom, prenom) VALUES (:login, :nom, :prenom)');

        $pdoStatement->execute([
            ':login' => $this->login,
            ':nom' => $this->nom,
            ':prenom' => $this->prenom
            ]);
    }

}
?>