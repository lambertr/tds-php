<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>

    <body>
        Voici le résultat du script PHP : 
        <?php
          // Ceci est un commentaire PHP sur une ligne
          /* Ceci est le 2ème type de commentaire PHP
          sur plusieurs lignes */
           
          // On met la chaine de caractères "hello" dans la variable 'texte'
          // Les noms de variable commencent par $ en PHP
          //$texte = "hello world !";

          // On écrit le contenu de la variable 'texte' dans la page Web
          //echo $texte;

        //$prenom = "Marc";
        /*
        echo "Bonjour\n " . $prenom;
        echo "Bonjour\n $prenom";
        echo 'Bonjour\n $prenom';

        echo $prenom;
        echo "$prenom";

        //tableaux
        $utilisateur = [
            'prenom' => 'Juste',
            'nom'    => 'Leblanc'
        ];
        //ajoout case tableaux
        $utilisateur['passion'] = 'maquettes en allumettes';

        //ajout élément a la fin du tableaux
        $utilisateur[] = "Nouvelle valeur";

        // Syntaxe avec {$...}
        echo "Je m'appelle {$utilisateur['nom']} <br>";
        echo "Je m'appelle {$utilisateur["nom"]} <br>";
        // Syntaxe simplifiée
        // Attention, pas de guillemets autour de la clé "nom"
        echo "Je m'appelle  $utilisateur[nom] <br>";

        // si tableaux indexé par des chaines de caractères
        foreach ($utilisateur as $cle => $valeur){
            echo "$cle : $valeur\n <br>";
        }
         si tableaux indexé par des entiers
        for ($i = 0; $i < count($utilisateur); $i++) {
            echo $utilisateur[$i];
        }
        */
        echo "Exercice 8 : <br>";
        //Q1
        $nom ='Lambert';
        $prenom='Raphaël';
        $login='lambertr';
        //Q2
        echo "Utilisateur $nom $prenom de login $login<br>";
        //Q3
        $utilisateur=[
                'nom' => 'Lambert',
                'prenom' => 'Raphaël',
                'login' => 'lambertr'
        ];
        //var_dump($utilisateur);
        echo "Utilisateur $utilisateur[nom] $utilisateur[prenom] de login $utilisateur[login]<br>";
        //Q4
        $utilisateurs=[
                'nico',
                'laulau',
                'francky'
        ];
        //var_dump($utilisateurs);
        if(count($utilisateurs)==0){
            echo "Il n'y a aucun utilisateur";
        } else {
            echo "Liste des utilisateurs: <br> <ul>";
            for ($i=0; $i < count($utilisateurs); $i++) {
                echo "<li>$utilisateurs[$i]";
            }
            echo "</ul>";
        }

        ?>
    </body>
</html> 