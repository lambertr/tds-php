<?php
require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';
use App\Covoiturage\Controleur\ControleurUtilisateur;
use App\Covoiturage\Modele\HTTP\Cookie;

// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');
if(isset($_GET['controleur'])){
    $controleur = $_GET['controleur'];
    $nomDeClasseControleur = "App\Covoiturage\Controleur\Controleur".ucfirst($controleur);
    if(class_exists($nomDeClasseControleur)){
        if(isset($_GET['action'])){
            $action = $_GET['action'];
            $exist = false;
            foreach (get_class_methods('App\Covoiturage\Controleur\ControleurUtilisateur') as $methode) {
                if($action == $methode){
                    $exist=true;
                    break;
                }
            }
            if(!$exist){
                $action = 'afficherErreur';
            }
        }else{
            $action = 'afficherListe';
        }
        $controleurInstance = new $nomDeClasseControleur();
        Cookie::enregistrer("first","cookie 1", time()+3600);
        $controleurInstance->$action();

    }else{
        $action = 'afficherErreur';
        ControleurUtilisateur::afficherErreur();
    }
}

?>