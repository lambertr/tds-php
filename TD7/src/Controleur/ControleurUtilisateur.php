<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Utilisateur;
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php',["utilisateurs"=>$utilisateurs,"titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);
    }
    public static function afficherDetail() : void {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
        if($utilisateur == NULL) {
            self::afficherErreur("L'utilisateur n'existe pas");
        } else{
            self::afficherVue('vueGenerale.php',['utilisateur' => $utilisateur,"titre" => "Detail de l'utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);
        }
    }
    public static function afficherFormulaireCreation() : void {
        self::afficherVue('vueGenerale.php',["titre" => "Formulaire de création", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }
    public static function afficherErreur(string $messageErreur = "") : void{
        self::afficherVue('vueGenerale.php',["messageErreur"=> $messageErreur ,"titre" => "Erreur", "cheminCorpsVue" => "utilisateur/erreur.php"]);
    }
    public static function creerDepuisFormulaire() : void
    {
        $nom = htmlspecialchars($_GET['nom']);
        $prenom = htmlspecialchars($_GET['prenom']);
        $login = htmlspecialchars($_GET['login']);
        $utilisateur = new Utilisateur($login, $nom, $prenom);
        $utilisateurRepository = new UtilisateurRepository();
        $utilisateurRepository->ajouter($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php',['utilisateurs' => $utilisateurs,"titre" => "Utilisateur créé","cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
    }
    public static function supprimer() : void{
        $login = htmlspecialchars($_GET['login']);
        (new UtilisateurRepository)->supprimer($login);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php',['utilisateurs' => $utilisateurs,'login' => $login, "titre" => "Utilisateur supprimer", "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php"]);
    }
    public static function afficherFormulaireMiseAJour() : void{
        $login = htmlspecialchars($_GET['login']);
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        self::afficherVue('vueGenerale.php', ['utilisateur' => $utilisateur, "titre" => "Mise à jour", "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php"] );
    }
    public static function mettreAJour() : void{
        $utilisateur = new Utilisateur($_GET['login'], $_GET['nom'], $_GET['prenom']);
        (new UtilisateurRepository)->mettreAJour($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        $login = htmlspecialchars($_GET['login']);
        self::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, 'login' => $login, "titre" => "Utilisateur Modifié", "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php"]);
    }
    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ .  "/../vue/$cheminVue"; // Charge la vue
    }
    /*
    public function deposerCookie(): void {
        Cookie::enregistrer('monCookie', 'valeurDeMonCookie', 3600);
    }
    public function lireCookie(): void {
        $valeur = Cookie::lire('monCookie');
        if ($valeur !== null) {
            echo "Valeur du cookie : " . $valeur;
        } else {
            echo "Le cookie n'existe pas.";
        }
    }
    */



}
?>