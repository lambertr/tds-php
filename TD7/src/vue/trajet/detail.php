<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Details des trajets</title>
</head>
<body>
<?php

/** @var Trajet[] $trajet */
$fumeurStatus = $trajet->isNonFumeur() ? 'fumeur' : 'non fumeur';
$depart = htmlspecialchars($trajet->getDepart(), ENT_QUOTES, 'UTF-8');
$arrivee = htmlspecialchars($trajet->getArrivee(), ENT_QUOTES, 'UTF-8');
$prenomConducteur = htmlspecialchars($trajet->getConducteur()->getPrenom(), ENT_QUOTES, 'UTF-8');
$nomConducteur = htmlspecialchars($trajet->getConducteur()->getNom(), ENT_QUOTES, 'UTF-8');

echo "<p>
            Le trajet est $fumeurStatus du {$trajet->getDate()->format("d/m/Y")} partira de $depart pour aller à $arrivee (conducteur: $prenomConducteur $nomConducteur).
        </p>";
?>
</body>
</html>