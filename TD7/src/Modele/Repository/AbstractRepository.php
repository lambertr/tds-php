<?php
namespace App\Covoiturage\Modele\Repository;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur;

abstract class AbstractRepository
{
    public function mettreAJour(AbstractDataObject $objet): void
    {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $nomTable = $this->getNomTable();
        $nomClePrimaire = $this->getNomClePrimaire();
        $nomsColonnes = $this->getNomsColonnes();
        $colonnesSet = join(', ', array_map(fn($colonne) => "$colonne = :{$colonne}Tag", $nomsColonnes));
        $sql = "UPDATE $nomTable SET $colonnesSet WHERE $nomClePrimaire = :{$nomClePrimaire}Tag";
        $pdoStatement = $pdo->prepare($sql);
        $values = $this->formatTableauSQL($objet);
        $pdoStatement->execute($values);
    }

    public function ajouter(AbstractDataObject $objet): bool
    {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $nomTable = $this->getNomTable();
        $nomsColonnes = $this->getNomsColonnes();
        $colonnes = join(', ', $nomsColonnes);
        $tags = join(', ', array_map(fn($colonne) => ':' . $colonne . 'Tag', $nomsColonnes));
        $sql = "INSERT INTO $nomTable ($colonnes) VALUES ($tags)";
        $pdoStatement = $pdo->prepare($sql);
        $values = $this->formatTableauSQL($objet);
        return $pdoStatement->execute($values);
    }

    public function supprimer($valeurClePrimaire): void
    {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "DELETE FROM " . $this->getNomTable() . " WHERE " . $this->getNomClePrimaire() . " = :valeurClePrimaire";
        $pdoStatement = $pdo->prepare($sql);
        $pdoStatement->execute([
            ':valeurClePrimaire' => $valeurClePrimaire
        ]);
    }


    public function recupererParClePrimaire(string $clePrimaire): ?AbstractDataObject
    {
        $sql = "SELECT * FROM " . $this->getNomTable() . " WHERE " . $this->getNomClePrimaire() . " = :clePrimaireTag";
        $pdoStatement = \App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "clePrimaireTag" => $clePrimaire,
        );
        $pdoStatement->execute($values);
        $objetFormatTableau = $pdoStatement->fetch();
        if ($objetFormatTableau) {
            return $this->construireDepuisTableauSQL($objetFormatTableau);
        }
        return null;
    }
    /**
     * Retrieve all rows from the table defined by getNomTable()
     *
     * @return AbstractDataObject[]
     */
    public function recuperer()
    {
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM " . $this->getNomTable());
        $tableauTable = [];
        $nomTableRepo = 'App\\Covoiturage\\Modele\\Repository\\' . ucfirst($this->getNomTable()) . 'Repository';
        foreach ($pdoStatement as $TableFormatTableau) {
            $table = ucfirst($nomTableRepo)::construireDepuisTableauSQL($TableFormatTableau);
            $tableauTable[] = $table;
        }
        return $tableauTable;
    }


    protected abstract function getNomTable(): string;
    protected abstract function getNomClePrimaire(): string;
    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau) : AbstractDataObject;
    /** @return string[] */
    protected abstract function getNomsColonnes(): array;
    protected abstract function formatTableauSQL(AbstractDataObject $objet): array;



}