<?php
namespace App\Covoiturage\Modele\Repository;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use DateTime;

class TrajetRepository extends AbstractRepository
{
    protected function construireDepuisTableauSQL(array $trajetTableau) : AbstractDataObject
    {
        $date = new DateTime($trajetTableau["date"]);
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($trajetTableau["conducteurLogin"]);

        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            $date,
            $trajetTableau["prix"],
            $utilisateur,
            $trajetTableau["nonFumeur"],
            []
        );

        $passagers = $trajet->getPassagers();

        $trajet->setPassagers($passagers);

        return $trajet;
    }

    /**
     * @return Trajet[]
     */
    /*
    public static function recupererTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajets[] = self::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }
    */
    /**
     * @return Utilisateur[]
     */
    static public function recupererPassagers(Trajet $trajet): array
    {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $pdoStatement = $pdo->prepare("SELECT * FROM utilisateur u JOIN passager p on u.login=p.passagerLogin WHERE p.trajetId='$trajet->getId()'");
        $pdoStatement->execute();
        $passagers=[];
        foreach ($pdoStatement as $passagerFormatTableau) {
            $passager=(new UtilisateurRepository())->construireDepuisTableauSQL($passagerFormatTableau);
            $passagers[]=$passager;
        }
        return $passagers;
    }

    protected function getNomTable(): string
    {
        return 'trajet';
    }

    protected function getNomClePrimaire(): string
    {
        return 'id';
    }
    /** @return string[] */
    protected function getNomsColonnes(): array
    {
        return ["id", "depart", "arrivee", "date", "prix", "conducteurLogin", "nonFumeur"];
    }
    protected function formatTableauSQL(AbstractDataObject $trajet): array
    {
        /** @var Trajet $trajet */
        return array(
            "idTag" => $trajet->getId(),
            "departTag" => $trajet->getDepart(),
            "arriveeTag" => $trajet->getArrivee(),
            "dateTag" => $trajet->getDate()->format("Y-m-d"),
            "prixTag" => $trajet->getPrix(),
            "conducteurLoginTag" => $trajet->getConducteur()->getLogin(),
            'nonFumeurTag' => $trajet->isNonFumeur() !== '' ? (int) $trajet->isNonFumeur() : 0
        );
    }
}