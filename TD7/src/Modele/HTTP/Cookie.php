<?php
namespace App\Covoiturage\Modele\HTTP;

class Cookie {

public static function enregistrer(string $cle, mixed $valeur, ?int $dureeExpiration = null): void {
$valeurStr = serialize($valeur);
    if ($dureeExpiration !== null) {
        setcookie($cle, $valeurStr, time() + $dureeExpiration, "/");
    } else {
        setcookie($cle, $valeurStr, 0, "/");
    }
}

public static function lire(string $cle): mixed {
    if (isset($_COOKIE[$cle])) {
        return unserialize($_COOKIE[$cle]);
    }
    return null;
}

public static function contient(string $cle): bool {
    return isset($_COOKIE[$cle]);
}
    public static function supprimer($cle) : void {
        if (isset($_COOKIE[$cle])) {
            setcookie($cle, '', time() - 3600, "/");
            unset($_COOKIE[$cle]);
        }
    }
}
