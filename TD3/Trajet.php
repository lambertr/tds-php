<?php

require_once 'ConnexionBaseDeDonnees.php';
require_once 'Utilisateur.php';

class Trajet {

    private ?int $id;
    private string $depart;
    private string $arrivee;
    private DateTime $date;
    private int $prix;
    private Utilisateur $conducteur;
    private bool $nonFumeur;
    /**
     * @var Utilisateur[]
     */
    private array $passagers;

    public function __construct(
        ?int $id,
        string $depart,
        string $arrivee,
        DateTime $date,
        int $prix,
        Utilisateur $conducteur,
        bool $nonFumeur
    )
    {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrivee = $arrivee;
        $this->date = $date;
        $this->prix = $prix;
        $this->conducteur = $conducteur;
        $this->nonFumeur = $nonFumeur;
        $this->passagers = [];
    }

    public static function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
        $date = new DateTime($trajetTableau["date"]);
        $utilisateur = Utilisateur::recupererUtilisateurParLogin($trajetTableau["conducteurLogin"]);

        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            $date,
            $trajetTableau["prix"],
            $utilisateur,
            $trajetTableau["nonFumeur"],
            []
        );

        $passagers = $trajet->getPassagers();

        $trajet->setPassagers($passagers);

        return $trajet;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getDepart(): string
    {
        return $this->depart;
    }

    public function setDepart(string $depart): void
    {
        $this->depart = $depart;
    }

    public function getArrivee(): string
    {
        return $this->arrivee;
    }

    public function setArrivee(string $arrivee): void
    {
        $this->arrivee = $arrivee;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    public function getPrix(): int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): void
    {
        $this->prix = $prix;
    }

    public function getConducteur(): Utilisateur
    {
        return $this->conducteur;
    }

    public function setConducteur(Utilisateur $conducteur): void
    {
        $this->conducteur = $conducteur;
    }

    public function isNonFumeur(): bool
    {
        return $this->nonFumeur;
    }

    public function setNonFumeur(bool $nonFumeur): void
    {
        $this->nonFumeur = $nonFumeur;
    }

    public function getPassagers(): array
    {
        return $this->recupererPassagers();
    }

    public function setPassagers(array $passagers): void
    {
        $this->passagers = $passagers;
    }


    public function __toString()
    {
        $nonFumeur = $this->nonFumeur ? " non fumeur" : " ";
        return "<p>
            Le trajet$nonFumeur du {$this->date->format("d/m/Y")} partira de {$this->depart} pour aller à {$this->arrivee} (conducteur: {$this->conducteur->getPrenom()} {$this->conducteur->getNom()}).
        </p>";
    }

    /**
     * @return Trajet[]
     */
    public static function recupererTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajets[] = Trajet::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }
    public function ajouter() : void
    {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $pdoStatement = $pdo->prepare('INSERT INTO trajet (depart, arrivee, date, prix, conducteurLogin, nonFumeur) VALUES (:depart, :arrivee, :date, :prix, :conducteurLogin, :nonFumeur)');
        if($this->nonFumeur){
            $nonFumeurInt=1;
        }else{
            $nonFumeurInt = 0;
        }
        $pdoStatement->execute([
            ':depart' => $this->depart,
            ':arrivee' => $this->arrivee,
            ':date' => $this->date->format("Y-m-d"),
            ':prix' => $this->prix,
            ':conducteurLogin' => $this->conducteur->getLogin(),
            ':nonFumeur' => $nonFumeurInt
        ]);
    }
    /**
     * @return Utilisateur[]
     */
    private function recupererPassagers() : array
    {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $pdoStatement = $pdo->prepare("SELECT * FROM utilisateur u JOIN passager p on u.login=p.passagerLogin WHERE p.trajetId='$this->id'");
        $pdoStatement->execute();
        $passagers=[];
        foreach ($pdoStatement as $passagerFormatTableau) {
            $passager=Utilisateur::construireDepuisTableauSQL($passagerFormatTableau);
            $passagers[]=$passager;
        }
        return $passagers;
    }
}